import requests
from django.core.exceptions import FieldError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.conf import settings
from main.models import MenuCard, UserMessage
from main.repo.menu_card import get_menu_list


def index(request):
    sort_field = request.GET.get('sort', 'id')
    try:
        menu_card_list = get_menu_list(sort_field)
    except FieldError:
        menu_card_list = get_menu_list('id')

    page = request.GET.get('page', 1)
    paginator = Paginator(menu_card_list, 10)
    try:
        menu_cards = paginator.page(page)
    except (PageNotAnInteger, EmptyPage):
        menu_cards = paginator.page(1)

    context = {
        'menu_cards': menu_cards,
        'sort_field': sort_field,
        'sort_by_name_asc': sort_field == 'name',
        'sort_by_name_desc': sort_field == '-name',
        'sort_by_num_dish_asc': sort_field == 'num_dish',
        'sort_by_num_dish_desc': sort_field == '-num_dish',
        'urlencode': request.GET.urlencode
    }
    return render(request, 'index.html', context)


def detail(request, card_id):
    menu_card = MenuCard.objects.get(id=card_id)
    dishes = menu_card.dish_set.all()

    context = {
        'menu_card': menu_card,
        'dishes': dishes,
    }
    return render(request, 'detail.html', context)


def report(request):
    return render(request, 'report.html', {})


def send_report(request):
    recaptcha_response = request.POST.get('g-recaptcha-response')
    url = settings.GOOGLE_RECAPTCHA_URL
    data = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
    }
    r = requests.post(url, data=data)
    result = r.json()
    if not result['success']:
        return HttpResponseRedirect('report')
    email = request.POST.get('email', None)
    text = request.POST['message']
    message = UserMessage(text=text, user_email=email)
    message.save()
    return HttpResponseRedirect('success')


def send_success(request):
    return render(request, 'success.html')
