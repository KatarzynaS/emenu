from django.db import models

from main.validation import validate_file_size

DATE_FORMAT='"%Y-%m-%d %H:%M:%S"'


def user_directory_path(instance, filename):
    return 'card{}/dish{}/{}'.format(instance.menu_card_id, instance.id, filename)


class MenuCard(models.Model):
    name = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.name


class Dish(models.Model):
    menu_card = models.ForeignKey(MenuCard, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    image = models.ImageField(upload_to=user_directory_path,
                              null=True,
                              validators=[validate_file_size])

    def __str__(self):
        return self.name


class UserMessage(models.Model):
    user_email = models.EmailField(max_length=100)
    text = models.CharField(max_length=500)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}-{}'.format(
            self.created_on.strftime(DATE_FORMAT),
            self.user_email or 'anonymous_user')