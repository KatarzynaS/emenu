from django.contrib import admin
from main.models import MenuCard, Dish, UserMessage


class UserMessageAdmin(admin.ModelAdmin):
    fields = ('user_email', 'text', 'created_on')
    readonly_fields = ('user_email', 'text', 'created_on')


admin.site.register(MenuCard)
admin.site.register(Dish)
admin.site.register(UserMessage, UserMessageAdmin)
