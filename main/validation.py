from django.conf import settings
from django.core.exceptions import ValidationError


def validate_file_size(file_obj):
    file_size = file_obj.file.size
    megabyte_limit = settings.MAX_UPLOAD_SIZE
    if file_size > megabyte_limit * 1024 * 1024:
        raise ValidationError("Max file size is %sMB" % str(megabyte_limit))