from django.db.models import Count
from main.models import MenuCard


def get_menu_list(sort_field):
    menu_card_list = MenuCard.objects.annotate(
        num_dish=Count('dish')
    ).filter(
        num_dish__gte=1
    ).order_by(
        sort_field
    )
    return list(menu_card_list)