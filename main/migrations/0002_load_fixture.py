from random import randint

from django.contrib.auth.models import User
from django.db import migrations

from main.models import MenuCard, Dish

SAMPLE_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "


def load_data(apps, schema_editor):
    menu_card_list = []
    for num in range(1, 101):
        card_name = 'Restaurant{}'.format(str(num))
        card = MenuCard(name=card_name, description=SAMPLE_TEXT)
        menu_card_list.append(card)

    MenuCard.objects.bulk_create(menu_card_list)

    dish_list = []
    inserted_cards = MenuCard.objects.all()
    for card in inserted_cards:
        for num in range(randint(0, 10)):
            dish_name = 'Dish{}'.format(str(num))
            price = randint(200, 5000) / 100
            dish = Dish(
                name=dish_name,
                description=SAMPLE_TEXT*2,
                menu_card_id=card.id,
                price=price
            )
            dish_list.append(dish)

    Dish.objects.bulk_create(dish_list)
    user = User(username="admin", is_active=True,
                is_superuser=True, is_staff=True,
                email="admin@gmail.com")
    user.set_password('BlueMedia')
    user.save()


def delete_data(apps, schema_editor):
    Dish.objects.all().delete()
    MenuCard.objects.all().delete()
    User.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_data, delete_data),
    ]
