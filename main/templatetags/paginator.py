from django import template

register = template.Library()


@register.inclusion_tag('components/paginator.html')
def paginator(paged, urlencode, adjacent_pages=2):
    paginator = paged.paginator
    start_page = max(paged.number - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1
    end_page = paged.number + adjacent_pages + 1
    if end_page >= paginator.num_pages - 1:
        end_page = paginator.num_pages + 1
    page_numbers = [n for n in range(start_page, end_page)
                    if 0 < n <= paginator.num_pages]

    return {
        'paged': paged,
        'paginator': paginator,
        'page': paged.number,
        'page_numbers': page_numbers,
        'has_next': paged.has_next(),
        'has_previous': paged.has_previous(),
        'show_first': 1 not in page_numbers,
        'show_last': paginator.num_pages not in page_numbers,
        'urlencode': urlencode
    }
