from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('detail/<int:card_id>/', views.detail, name='detail'),
    path('report', views.report, name='report'),
    path('send_report', views.send_report, name='send_report'),
    path('success', views.send_success, name='send_success')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)