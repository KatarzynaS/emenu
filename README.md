### Prerequisites

Python 3.4

### Installing and starting the project

#### 1. Create python virtual environment

virtualenv -p python3 venv_emenu  
source venv_emenu/bin/activate


#### 2. Install required dependencies

In project root directory run:  
pip install -r requirements.txt


#### 3. Migrate sqlite db

In project root directory run:  
python manage.py migrate


#### 4. Run server

In project root directory run:  
python manage.py runserver


#### 5. Admin site
Admin site can be found at /admin/. Login credentials:  
 user: admin, password: BlueMedia


